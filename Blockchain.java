

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.lang.Object;

import org.apache.commons.codec.digest.DigestUtils;
//import java.nio.charset.StandardCharsets;

/**
 * Created by adam on 5/4/17.
 */
public class Blockchain {
    private ArrayList<Block> blockchain;
    private static int blockchainCount;

    public Blockchain() {

        // Create new blockchain
        blockchainCount++;
        blockchain = new ArrayList<Block>();
        System.out.println("I created a blockchain!");

        // Create and add new block to the blockchain.
        this.addNewBlock();





//        System.out.println("Printing blockchain : ");
//        System.out.println(blockchain);

    }

    public String toString () {
        String retString = new String();
        for (Block b : blockchain) {
            retString += b.toString();
        }
        return retString;
    }

    private void addNewBlock () {
        Block b;
        if (blockchain.size()== 0 ) {
            b = new Block("NOTHING", 0);

        } else {
            b = new Block(getCurrentBlock().getHash(), getCurrentBlock().blockNum + 1);

        }
        blockchain.add(b);
    }

    private Block getCurrentBlock () {
        return blockchain.get(blockchain.size() - 1);
    }

    public boolean addData (String s) {
        Block currBlock = getCurrentBlock();
        if (currBlock.hasSpace()) {
            currBlock.addData(s);
        } else {
            addNewBlock();
            getCurrentBlock().addData(s);
        }




        return true;
    }

    private class Block {
        private int blockNum;
        private int nonce;
        private ArrayList<String> data;
        private String previousBlockHash;
        private String hash;

        public Block (String PrevousBlockHash, int blockNum){
            System.out.println("Yay. A new block is created.");
            this.previousBlockHash = PrevousBlockHash;
            this.blockNum = blockNum;
            this.data = new ArrayList<String>();
        }

        public void addData (String s){
            this.data.add(s);
        }

        public boolean hasSpace() {
            boolean spaceAvailable = false;
            if (data.size() < 4 ){
                spaceAvailable = true;
            }

            return spaceAvailable;

        }

        public String getHash () {
            return hashBlock();
        }

        private String hashBlock () {

            String ret = DigestUtils.sha1Hex(data.toString());

            return ret;

        }

        public String toString() {
            String ret =
//                    "-- Block #" + blockNum + " --\n" +
                    "\n" +
                    " | Block No :" + blockNum + "\n" +
                    " | Prev Block Hash : " + previousBlockHash + "\n" +
                    " | Hash : " + getHash() + "\n" +
                    " | Content : " + "\n" +
                    "";

            for (String s : data) {
                ret += "    " + s + "\n";
//                System.out.println("I am iterating");
            }

            return ret;
        }
    }

}
