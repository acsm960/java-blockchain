public class Main {

    public static void main(String[] args) {
	// write your code here
        Blockchain b = new Blockchain();
        b.addData("Pizze!");
        b.addData("Cheese");
        b.addData("Pizza");
        b.addData("Fun");
        b.addData("Jasper");
        b.addData("Holly");
        b.addData("Pupper");
        b.addData("Dogo");
        b.addData("Puppito");

        System.out.println(b);
    }
}
